# Listen to Learn

This project focuses on learning roads of disabled people, particularly people with poor eyesight and blind eyes. 

## Flow of the project
1. Take the voice command of the user on click of a button and convert it to text.
2. Use the web to get open source articles upon the topic asked by the user.
3. If the article is very huge to be read whole, generate summary of the article using NLP.
4. Display the summary of the article on the webpage and narrate using text to speech script.

## Technologies used
1. Python
2. Flask
3. HTML
4. CSS
5. JavaScript
6. Jinja
7. Natural Language Toolkit
8. Global Vectors for word representation (GloVe)
9. SpaCy library

## Algorithms and methods for Summary generation
The type of NLP approach we are using is called Extraction-based approach. In this approach a subset of words that represent the most important points is pulled from a piece of text and combined to make a summary. 
Using the GloVe the statistics of the word occurrences in a corpus is the primary source of information available to all the unsupervised methods for learning word representations.<br>
We use the GloVe because it gives the vectored representation of the words which is used to find similarity between different forms of a word. For this we use ***Cosine similarity*** to find how much a word or a sentence differes from each other and whether it should be included in the summary or not.

We are using punkt for tokenizing the text into a list of sentences. It uses an unsupervised algorithm to build a model for words and sentences. Then we do  text processing to filter the stop words. GloVe an unsupervised learning algorithm is used for obtaining vector representations of words. Then using cosine similarity for vectors we create rankings from networkx python module, used for determining patterns.

We are using the spaCy pipelines such as en_core_web_sm which is a small english pipeline with vocabulary, vectors, syntax and entities.
We rank the preprocessed words to conbine them eventually to make an extractive summary. For this we use the NetworkX to make a network graphs of words and sentences as it provides for manipulation, structure and dynamics of a graph and complex networks.

The glove.6B.100d.txt contains 6 billion tokens and 100 features of texts represented in respective vector formats.

We hope this minimum viable project which can have a huge scope in educating the blind people will be helpful and used for a good cause.
