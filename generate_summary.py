import nltk
import os
import re
import spacy
import sys
import networkx as nx
import numpy as np
import pandas as pd 

nltk.download('punkt')
nltk.download('stopwords')

from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize
from sklearn.metrics.pairwise import cosine_similarity

root = '.'
# Parts-of-Speech tagging.
nlp_spacy = spacy.load('en_core_web_sm')
# stopwords
stop_words = stopwords.words('english')

def remove_extraneous_text(sentence:str)->str:
	sentence = re.sub(" +", " ", sentence)

	if ") --" in sentence:
		sentence = sentence.split(") --")[-1]

	return sentence

def remove_stopwords(sentence:str)->str:
	sentence = " ".join([word for word in sentence.split() if word not in stop_words])
	return sentence

def lemmatize_text(sentence:str)->str:

	sentence = nlp_spacy(sentence)
	sentence = ' '.join([word.lemma_ if word.lemma_ != "-PRON-" else word.text for word in sentence])
	return sentence

def clean_text(sentence:str)->str:
	sentence = sentence.lower()
	sentence = re.sub("[^a-zA-Z]", " ", sentence)
	sentence = remove_extraneous_text(sentence)
	sentence = remove_stopwords(sentence)
	sentence = lemmatize_text(sentence)
	return sentence

def get_total_terms(cleaned_sentences:list)->int:
	total_terms = 0
	for sentence in cleaned_sentences:
		total_terms += len(sentence.split())

	return total_terms

def get_term_frequencies(cleaned_sentences:list)->dict:
	freq_dict = {}
	for sentence in cleaned_sentences:
		for word in sentence.split():
			freq_dict[word] = freq_dict.get(word, 0) + 1

	return freq_dict

def get_term_weights(cleaned_sentences:list)->dict:
	total_terms = get_total_terms(cleaned_sentences)
	term_freq_dict = get_term_frequencies(cleaned_sentences)
	term_weights = dict()

	for key, value in term_freq_dict.items():
		term_weights[key] = (value * 1000) / total_terms

	return term_weights

def inverse_sentence_frequency(cleaned_sentences:list)->dict:
	vocabulary = set()

	for sentence in cleaned_sentences:
		vocabulary = vocabulary.union(set(sentence.split()))

	isf = dict()
	number_of_sentences = len(cleaned_sentences)

	for word in vocabulary:
		number_of_appearances = 0

		for sentence in cleaned_sentences:
			if word in sentence:
				number_of_appearances += 1

		isf[word] = np.log(number_of_sentences / number_of_appearances)

	return isf

def word_weights(cleaned_sentences:str)->dict:
	term_weights = get_term_weights(cleaned_sentences)
	inverse_sentence_freq = inverse_sentence_frequency(cleaned_sentences)

	resultant_weights = dict()

	for word in term_weights.keys():
		resultant_weights[word] = term_weights[word] * inverse_sentence_freq[word]

	return resultant_weights

def pos_tagging(cleaned_sentences:list)->list:
	tagged_sentences = []
	for sentence in cleaned_sentences:
		sentence_nlp = nlp_spacy(sentence)
		tagged_sentence = []
		for word in sentence_nlp:
			tagged_sentence.append((word, word.pos_))
		tagged_sentences.append(tagged_sentence)

	return tagged_sentences

def sentence_weights(tagged_sentences:list, total_terms:int)->list:
	sent_weights = []
	for sentence in tagged_sentences:
		relevance_count = 0
		for word, tag in sentence:
			if tag == 'NOUN' or tag == 'VERB':
				relevance_count += 1
		sent_weights.append(relevance_count / total_terms)

	return sent_weights

def sentence_position(cleaned_sentences:list)->list:
	sent_position = []
	number_of_sentences = len(cleaned_sentences)
	weights = [0, 0.25, 0.23, 0.14, 0.08, 0.05, 0.04, 0.06, 0.04, 0.04, 0.15]
	for i in range(1, len(cleaned_sentences)+1):
		sent_position.append(weights[int(np.ceil(10 * (i / number_of_sentences)))])

	return sent_position

def sentence_length(cleaned_sentences:list)->list:
	sent_len = []
	for sentence in cleaned_sentences:
		sent_len.append(len(sentence.split()))

	return sent_len





# Functions to rank sentences.
def text_rank(sentences:list, word_embeddings:dict)->dict:
	clean_sentences = pd.Series(sentences).str.replace("[^a-zA-Z]", " ")
	clean_sentences = [s.lower() for s in clean_sentences]
	clean_sentences = [remove_stopwords(r) for r in clean_sentences]
	sentence_vectors = []
	for i in clean_sentences:
		if len(i) != 0:
			v = sum([word_embeddings.get(w, np.zeros((100, ))) for w in i.split()]) / (len(i.split()) + 0.001)
		else:
			v = np.zeros((100, ))
		sentence_vectors.append(v)

	sim_mat = np.zeros([len(sentences), len(sentences)])
	for i in range(len(sentences)):
		for j in range(len(sentences)):
			if i != j:
				sim_mat[i][j] = cosine_similarity(sentence_vectors[i].reshape(1, 100), sentence_vectors[j].reshape(1, 100))[0, 0]
	nx_graph = nx.from_numpy_array(sim_mat)
	scores = nx.pagerank(nx_graph)

	return scores

def feature_rank(sentences:list)->dict:
	cleaned_sentences = [clean_text(sentence) for sentence in sentences]
	term_weights = word_weights(cleaned_sentences)
	tagged_sentences = pos_tagging(cleaned_sentences)
	total_terms = get_total_terms(cleaned_sentences)
	sent_weights = sentence_weights(tagged_sentences, total_terms)
	sent_position = sentence_position(cleaned_sentences)
	sent_len = sentence_length(cleaned_sentences)

	sentence_scores = []
	for index, sentence in enumerate(cleaned_sentences):
		score = 0
		for word in sentence.split():
			score += term_weights[word]
		score *= sent_weights[index]
		score += sent_position[index]
		if sent_len[index] != 0:
			score /= sent_len[index]
		else:
			score = 0
		sentence_scores.append(score)
	sentence_scores = sentence_scores / np.sum(sentence_scores)
	final_scores = dict()
	for i in range(len(sentence_scores)):
		final_scores[i] = sentence_scores[i]

	return final_scores

# The driver function
def summarize(raw_text):
	print(len(raw_text))
	# Location of Glove word embeddings.
	glove_location = os.path.join(root, 'glove.6B.100d.txt')
	if not os.path.exists(glove_location):
		print("Could not find Glove Word Embeddings. Kindly download from 'https://drive.google.com/open?id=1cQBYwoLHZzHk4w8zdgcSPFmOP5Xq-x0z' \
			and save in './embeddings' location.")
		return
	print("Loading Glove Word embeddings.")
	word_embeddings = {}
	f = open(glove_location, encoding='utf-8')
	for line in f:
		values = line.split()
		word = values[0]
		coefs = np.asarray(values[1:], dtype='float32')
		word_embeddings[word] = coefs
	f.close()
	print("Embeddings loaded.")
	print("Creating summary.")

	sentences = sent_tokenize(raw_text)
	text_rank_scores = text_rank(sentences, word_embeddings)
	feature_rank_scores = feature_rank(sentences)
	final_scores = dict()
	for i in range(len(text_rank_scores.keys())):
		final_scores[i] = 0.8 * text_rank_scores[i] + 0.2 * feature_rank_scores[i]
	ranked_sentences = sorted(((final_scores[i], s, i) for i, s in enumerate(sentences)), reverse=True)[:3]
	ranked_sentences = sorted(ranked_sentences, key=lambda x: x[2])

	summary= ""
	for i in range(len(ranked_sentences)):
		summary += ranked_sentences[i][1] + ' '
	print("Our made summary is...............------------------------=====================\n\n\n")
	print(len(summary))
	return summary

# text = "In 1969, the children's television show Sesame Street premiered on the National Educational Television network (later succeeded by PBS) in the United States. Unlike earlier children's programming, the show's producers used research and over 1,000 studies and experiments to create the show and test its impact on its young viewers' learning. By the end of the program's first season, Children's Television Workshop (CTW), the organization founded to oversee Sesame Street production, had developed what came to be called the CTW model a system of planning, production, and evaluation that combined the expertise of researchers and early childhood educators with that of the program's writers, producers, and directors. CTW conducted research in two ways: in-house formative research that informed and improved production, and independent summative evaluations conducted by the Educational Testing Service (ETS) during the show's first two seasons to measure the program's educational effectiveness. CTW researchers invented tools to measure young viewers' attention to the program. Based on these findings, the researchers compiled a body of data and the producers changed the show accordingly. Summative research conducted over the years, including two landmark evaluations in 1970 and 1971, demonstrated that viewing the program had positive effects on young viewers' learning, school readiness, and social skills. Subsequent studies have replicated these findings, such as the effect of the show in countries outside of the U.S., several longitudinal studies, the effects of war and natural disasters on young children, and studies about how the show affected viewers' cognition. CTW researcher Gerald S. Lesser stated in 1974 that early tests conducted on the show (both formative and summative) suggested that Sesame Street was making strides towards teaching what it had set out to teach. According to author Louise A. Gikow, Sesame Street's use of research to create individual episodes and to test its effect on its young viewers set it apart from other children's programming. Co-creator Joan Ganz Cooney called the idea of combining research with television production positively heretical because it had never been done before. Before Sesame Street, most television shows were locally produced, with hosts who, according to researchers Edward L. Palmer and Shalom M. Fisch, represented the scope and vision of a single individual and were often condescending to their audience. Scriptwriters of these shows had no training in education or child development. The Carnegie Corporation, one of Sesame Street's first financial backers, hired Cooney, a producer of educational talk shows and documentaries with little experience in education, during the summer of 1967 to visit experts in childhood development, education, and media across the U.S. and Canada. She researched their ideas about the viewing habits of young children, and wrote a report on her findings entitled Television for Preschool Education, which described out how television could be used as an aid in the education of preschoolers, especially those living in inner cities and became the basis for Sesame Street. Full funding was procured for the development and production of the new show, and for the creation of the Children's Television Workshop (CTW), the organization responsible for producing it. The show's financial backers, which consisted of the U.S. federal government, the Corporation for Public Broadcasting and the Ford Foundation, insisted on testing at critical stages to evaluate its ultimate success. During the summer of 1968, Gerald S. Lesser, CTW's first advisory-board chairman, conducted five three-day curriculum-planning seminars in Boston and New York City to select a curriculum for the new program. Seminar participants were television producers and child development experts. It was the first time a children's television show used a curriculum, which Palmer, who was responsible for conducting the show's formative research, and Fisch described as detailed or stated in terms of measurable outcomes. The program's creative staff was concerned that this goal would limit creativity, but one of the seminar results was to encourage the show's producers to use child-development concepts in the creative process. Some Muppet characters were created during the seminars to fill specific curriculum needs. For example, Oscar the Grouch was designed to teach children about their positive and negative emotions, and Big Bird was created to provide children with opportunities to correct his bumbling mistakes. Lesser reported that Jim Henson had a particular gift for creating scenes that might teach. The show's research staff and producers conducted regularly-scheduled internal reviews and seminars to ensure that their curriculum goals were being met and to guide future production. As of 2001, ten seminars had been conducted specifically to address the literacy needs of preschool children. Curriculum seminars prior to Sesame Street's 33rd season in 2002 resulted in a change from the show's magazine-like format to a more narrative format. There have been over 1,000 studies as of 2001 which examine the show's impact on children's learning and attention. Most of these studies were conducted by the CTW and remain unpublished. Educator Herbert A. Sprigle and psychologist Thomas D. Cook conducted two studies during the show's first two seasons that found that the show increased the educational gap between poor and middle-class children, although these studies had little impact on the public discussion about Sesame Street. Another criticism of the show was made by journalist Kay Hymowitz in 1995, who reported that most of the positive research conducted on the show has been done by the CTW, and then sent to a sympathetic press. She charged that the studies conducted by the CTW hint at advocacy masquerading as social science."
# summarize(text)