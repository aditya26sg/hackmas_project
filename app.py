from flask import Flask, render_template, redirect, url_for, request
from bs4 import BeautifulSoup
from urllib.request import urlopen
import re

app = Flask(__name__)


@app.route('/')
@app.route('/home', methods=['GET', 'POST'])
def homepage():
    data = False
    if request.method == "POST":
        form = request.form['q']
        summary_data = extract_text_article(form)
        return render_template('inno.html', data=summary_data)
    return render_template('inno.html')


def extract_text_article(query):
    url = "_".join(query.split())
    wiki_search = 'https://en.wikipedia.org/wiki/'+url
    page = urlopen(wiki_search)
    soup = BeautifulSoup(page)
    fetched_text = ' '.join(map(lambda p: p.text, soup.find_all('p')))
    updated_fetched_text = re.sub("\[[0-9]+\]", '', fetched_text)
    print(len(updated_fetched_text))
    if len(updated_fetched_text) > 500:
        from generate_summary import summarize
        if len(updated_fetched_text) > 20000:
            updated_fetched_text = updated_fetched_text[:20000]
        return summarize(updated_fetched_text)
    else:
        return updated_fetched_text


@ app.route('/admin')
def admin():
    return redirect(url_for("homepage"))


@app.route('/login', methods=['POST', 'GET'])
def login():
    return render_template()


@app.route("/<usr>")
def user(usr):
    return f"<h1>{usr}</h1>"


if __name__ == "__main__":
    app.run(debug=True)
